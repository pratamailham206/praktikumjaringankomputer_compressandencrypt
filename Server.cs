﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;
using System.Security.AccessControl;


namespace Server
{
    class Program
    {       

        private static void ProcessClientRequests(object argument)
        {
            TcpClient server = (TcpClient)argument;
            try
            {
                //create TCPStream
                StreamReader reader = new StreamReader(server.GetStream());
                StreamWriter writer = new StreamWriter(server.GetStream());

                //START DECOMPRESS
                string filePath = @"D:\Project\Jarkom3\Compression\Server\Server\image";
                string compressedPath = @"D:\Project\Jarkom3\Compression\Server\Server\image_compress.zip";
                
                Console.WriteLine("CONNECTED.!");
                Console.WriteLine();
                Console.WriteLine("----Compress File----");
                Console.WriteLine("Start Compressing your file..!!!");

                //COMPRESSING FILE
                ZipFile.CreateFromDirectory(filePath, compressedPath);
                Thread.Sleep(1000);
                Console.WriteLine("Success..!!");
                Console.WriteLine();

                //START ENCRYPT
                Console.WriteLine("----Encrypt File----");         
                string encryptedFile = @"D:\Project\Jarkom3\Compression\Server\Server\image_compress_encrypt.zip";

                //ENCRYPTING FILE                
                EncryptFile(compressedPath, encryptedFile);
                Console.WriteLine("Result : " + encryptedFile);
                Console.WriteLine();

                //SEND FILE
                Console.Write("----Send File----");
                Console.WriteLine("Start Sending file : ");                
                Console.WriteLine("File to send : " + encryptedFile);

                Console.WriteLine("Sending file.");             

                byte[] bytes = File.ReadAllBytes(encryptedFile);

                //SEND SIZE
                writer.WriteLine(bytes.Length.ToString());
                writer.Flush();

                //SEND NAME & FILE
                writer.WriteLine(encryptedFile);
                writer.Flush();
                
                server.Client.SendFile(encryptedFile);
                Console.WriteLine("File Sent.!");

                server.Close();
                reader.Close();
                writer.Close();
            }
            catch (IOException)
            {
                Console.WriteLine("Exiting thread.");
            }
            finally
            {
                if (server != null)
                {
                    server.Close();
                }
            }
        }        

        public static void Main(string[] args)
        {
            TcpListener listener = null;
            try
            {
                listener = new TcpListener(IPAddress.Parse("127.0.0.1"), 8080);
                listener.Start();
                Console.WriteLine("Server started...");
                while (true)
                {
                    Console.WriteLine("Waiting for incoming client connections...");
                    TcpClient client = listener.AcceptTcpClient();                
                    Console.WriteLine("Accepted new client connection...");
                    Thread t = new Thread(ProcessClientRequests);
                    t.Start(client);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            finally
            {
                if (listener != null)   
                {
                    listener.Stop();
                }
            }
        }

        //USING SYMMETRIC ENCRYPTION (Rijndael)
        public static void EncryptFile(string inputFile, string outputFile)
        {
            try
            {
                string password = @"myKey123";
                UnicodeEncoding UE = new UnicodeEncoding();
                byte[] key = UE.GetBytes(password);
            
                string cryptFile = outputFile;
                FileStream fsCrypt = new FileStream(cryptFile, FileMode.Create);
                RijndaelManaged RMCrypto = new RijndaelManaged();
                CryptoStream cs = new CryptoStream(fsCrypt, RMCrypto.CreateEncryptor(key, key), CryptoStreamMode.Write);            
                FileStream fsIn = new FileStream(inputFile, FileMode.Open);

                int data;
                while ((data = fsIn.ReadByte()) != -1)
                    cs.WriteByte((byte)data);

                fsIn.Close();
                cs.Close();
                fsCrypt.Close();
            }
            catch
            {
                Console.WriteLine("Encryption Failed");
            }
        }
    }
}

