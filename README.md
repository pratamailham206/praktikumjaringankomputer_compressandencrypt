# PraktikumJaringanKomputer_CompressAndEncrypt

### **Server Client Program to Compress and Encrypt File**

nama : Ilham Pratama (4210181020)

#### **STEP** (Compress -> Encrypt -> Send -> Decrypt -> Extract)

#### **Server Start Connection** ->

wait for client connection

#### **Client Connect** ->

#### **Compress file** ->

create folder which will be used to compress

set using preference :
* system.IO.Compression;
* system.IO.Compression.fileSystem

set file directory, and output file. output is a Zip File

#### **Encrypt File** ->

(using Symmetric(Rijndael))

create function to Encrypt zip file.

func Encrypt with parameter(zipFile and outputEncryptedFile)

#### **Send File to Client** ->

item we need to send :
1.  fileName (EncryptedZip)
2.  fileSize
3.  file (EncryptedZip) from directory

#### **Client Receive File** ->

Receive fileName, fileSize.

Declare Buffer, read, and Size

Client buffer read received EncryptedFile

file saved in default directory (ProjectName\bin\Debug)

#### **Decrypting FileReceived** ->

create func to Decrypt file

set parameter to(zipEncryptedFile, DecryptedFile)

zipEncrypted is received file

the output is Decypted file with zip format

now file can open

#### **Extract file** ->

set file to extract

declare directory where extracted file

set using preference :
* system.IO.Compression;
* system.IO.Compression.fileSystem

the output is all files in the zip that we compressed before.

#### Close Connection