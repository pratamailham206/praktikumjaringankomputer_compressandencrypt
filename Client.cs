﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.IO;
using System.IO.Compression;

using System.Security.Cryptography;

namespace Client
{
    class Program
    {
        public static void Main(string[] args)
        {
            TcpClient client = new TcpClient("127.0.0.1", 8080);
            try
            {
                StreamReader reader = new StreamReader(client.GetStream());
                StreamWriter writer = new StreamWriter(client.GetStream());

                Console.WriteLine("Connected to Server.!");

                //RECEIVE SIZE
                string fileRecv_size = reader.ReadLine();

                //RECEIVE FILENAME
                Console.WriteLine("----Save File----");
                string fileRecv_name = reader.ReadLine();
                Console.WriteLine("Press Enter to Receive.!");

                Console.WriteLine(fileRecv_name + "size" + fileRecv_size);
                int length = Convert.ToInt32(fileRecv_size);
                byte[] buffer = new byte[length];
                int received = 0;
                int read = 0;
                int size = 1024;
                int remaining = 0;

                //READ
                while (received < length)
                {
                    remaining = length - received;
                    if (remaining < size)
                    {
                        size = remaining;
                    }
                    read = client.GetStream().Read(buffer, received, size);
                    received += read;
                }

                //SAVE FILE
                using (FileStream fStream = new FileStream(Path.GetFileName(fileRecv_name), FileMode.Create))
                {
                    fStream.Write(buffer, 0, buffer.Length);
                    fStream.Flush();
                    fStream.Close();
                }                

                Console.WriteLine("File received and saved in " + Environment.CurrentDirectory);

                //START DECRYPT
                Console.WriteLine();
                Console.WriteLine("----Decrypting File----");
                Console.WriteLine("Start Decrypting File..!");           
                Console.WriteLine(fileRecv_name);
                string decryptedFile = "image_decrypted.zip";

                //DECRYPT
                DecryptFile(fileRecv_name, decryptedFile);
                Console.WriteLine();
                

                //EXTRACT FILE
                Console.WriteLine("----Extract File----");
                string zipDecrypted = Environment.CurrentDirectory + @"\" + decryptedFile;
                string extractPath = Environment.CurrentDirectory;
                ZipFile.ExtractToDirectory(zipDecrypted, extractPath);
                Console.WriteLine("File Extracted to : " + extractPath);
                Console.WriteLine("Extract Succes.!");
                Console.ReadLine();

                client.Close();
                reader.Close();
                writer.Close();
            }

            catch (IOException)
            {
                Console.WriteLine("Error Connection.!");
            }
        }

        public static void DecryptFile(string inputFile, string outputFile)
        {
            {
                string password = @"myKey123";
                UnicodeEncoding UE = new UnicodeEncoding();
                byte[] key = UE.GetBytes(password);

                FileStream fsCrypt = new FileStream(inputFile, FileMode.Open);
                RijndaelManaged RMCrypto = new RijndaelManaged();
                CryptoStream cs = new CryptoStream(fsCrypt, RMCrypto.CreateDecryptor(key, key), CryptoStreamMode.Read);
                FileStream fsOut = new FileStream(outputFile, FileMode.Create);

                int data;
                while ((data = cs.ReadByte()) != -1)
                    fsOut.WriteByte((byte)data);

                fsOut.Close();
                cs.Close();
                fsCrypt.Close();
            }
        }
    }
}
